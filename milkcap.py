#!/usr/bin/env python3

import json
import os
import shutil
import subprocess
import sys


def read_settings():
    json_string = []
    with open("settings.json", "r") as f:
        for line in f.readlines():
            if not line.strip().startswith("//"):
                json_string.append(line)
    json_settings = json.loads("".join(json_string))

    # expand paths
    source_dirs = []
    for source_dir in json_settings['sources']:
        if type(source_dir) is dict:
            source_dir['path'] = os.path.abspath(source_dir['path'])
            source_dirs.append(source_dir)
        elif type(source_dir) is str:
            source_dirs.append({'path': os.path.abspath(source_dir)})
        else:
            raise TypeError("source_dirs accept str or dict")

    json_settings["sources"] = source_dirs
    json_settings["target"] = os.path.abspath(json_settings["target"])

    return json_settings


def find_directory(directory, settings):
    found_directory = None
    for setting in settings:
        if directory == setting['path'] and 'directory' in setting:
            found_directory = setting['directory']
    return found_directory


def git_command(args, cwd):
    git = subprocess.run(args,
                         stdout=subprocess.PIPE,
                         stderr=subprocess.PIPE,
                         universal_newlines=True,
                         cwd=cwd)
    try:
        git.check_returncode()
        return git.stdout.strip()
    except subprocess.CalledProcessError:
        from sys import stderr
        print(git.stderr, file=stderr)
        sys.exit()


def initialize_target_repository(initialized_path):
    os.makedirs(initialized_path, exist_ok=True)
    git_command(["git", "init"], initialized_path)


def commitlogs(cwd):
    output = git_command(["git", "--no-pager", "log",
                          "--oneline", "--pretty=format:%h\t%aD\t%at\t%s"],
                         cwd)
    return output


def fetch_commits(commits, target_dir):
    newlist = []
    for commit in commits.splitlines():
        commit_id, date_time, unix_time, body = commit.split("\t")
        newlist.append({'target_dir': target_dir,
                        'commit_id': commit_id,
                        'date_time': date_time,
                        'unix_time': unix_time,
                        'body': body})
    return newlist


def checkout_to_commit_id(cwd, commit_id):
    output = git_command(["git", "checkout", commit_id],
                         cwd)
    return output


def is_rebasing_branch(cwd, branch='rebasing'):
    output = git_command(["git", "rev-parse", "--abbrev-ref", "HEAD"],
                         cwd)
    return output == branch


def checkout_rebasing_branch(cwd, commit_id, branch='rebasing'):
    output = git_command(["git", "checkout", "-b", branch, commit_id],
                         cwd)
    return output


def create_format_patch(cwd, commit_id):
    output = git_command(["git", "format-patch", "-1", commit_id],
                         cwd)
    return output


def copy_format_patch(source_dir, target_dir, patch_name):
    source_path = os.path.join(source_dir, patch_name)
    target_path = os.path.join(target_dir, patch_name)
    shutil.copyfile(source_path, target_path)
    return target_path


def apply_format_patch(cwd, patch_path, directory=None):
    git_args = ["git", "am", "--committer-date-is-author-date"]
    if directory is not None:
        git_args += ["--directory", directory]
    output = git_command(git_args + [patch_path],
                         cwd)
    return output


def rebase_patched_branch(cwd, branch='rebasing'):
    output = git_command(["git", "rebase", branch],
                         cwd)

    return output


def delete_rebasing_branch(cwd, branch='rebasing'):
    output = git_command(["git", "branch", "-D", branch],
                         cwd)
    return output


def clean_target_directories(*cwds):
    for cwd in cwds:
        git_command(["git", "clean", "-f"], cwd)


def checkout_to_master(cwd):
    checkout_to_commit_id(cwd, 'master')


def main():
    settings = read_settings()
    base_dir = settings['target']
    source_dirs = [source_dir['path'] for source_dir in settings['sources']]

    commits = []
    for source_dir in source_dirs:
        output = commitlogs(source_dir)
        source_commits = fetch_commits(output, source_dir)
        commits = commits + source_commits

    sorted_commits = sorted(commits,
                            key=lambda x: x['unix_time'], reverse=False)

    initialize_target_repository(base_dir)

    for commit in sorted_commits:
        commit_id = commit['commit_id']
        target_dir = commit['target_dir']
        patch_name = create_format_patch(target_dir, commit_id)
        patch_path = copy_format_patch(target_dir, base_dir, patch_name)

        if find_directory(target_dir, settings['sources']) is None:
            print("root {} --> {}".format(target_dir, commit['body']))
            apply_format_patch(base_dir, patch_path)
        else:
            directory = find_directory(target_dir, settings['sources'])
            print("{} {} --> {}".format(directory, target_dir, commit['body']))
            apply_format_patch(base_dir, patch_path, directory)

        # remove patches
        clean_target_directories(base_dir, target_dir)


if __name__ == "__main__":
    main()
