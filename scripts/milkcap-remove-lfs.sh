#!/bin/bash

set -e

pushd "$1"

git lfs uninstall

git filter-branch --tree-filter '
    # remove lfs files
    remove_list=$(git lfs ls-files | cut -d" " -f3)
    for f in $remove_list; do
        if [ -f "$f" ]; then
            rm "$f"
        fi
    done

    # remove attributes
    if [ -f .gitattributes ]; then
        rm .gitattributes
    fi
' --prune-empty -f HEAD

popd
