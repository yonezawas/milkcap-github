# milkcap <img src="misc/dab0904ac4c5275f00a647e20ced3b4d.jpg" align="right" width="400">

Merge two git repositories into one.

## Important notice

**Git LFS is not supported** because they don't allow `git am` command with Git LFS files. If your repository has any Git LFS files, backup your file first. Use at your own risk.

## How to use

    # copy and rename to "settings.json"
    $ cp settings.example.json settings.json

    # edit settings.json
    $ nano settings.json

    $ python3 milkcap.py

    # if you hit an error, try milkcap-remove-lfs.sh below.

    # danger, this wipe all lfs files out.
    $ bash ./scripts/milkcap-remove-lfs.sh

    # open interactive and then close
    $ git rebase -i --root --committer-date-is-author-date

## Author

**milkcap** © [nzwsch](https://github.com/nzwsch).
Authored and maintained by Seiichi Yonezawa.
